﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace palindrome
{
  public class palindrome
  {
    
    public static bool validatePalindrome(string strExample)
    {
      // Remove all spaces from a string and force the string to uppercase
      string newS = (strExample.Replace(" ", "")).ToUpper();
      // Extract the first half of the original string.
      string first = newS.Substring(0, newS.Length / 2);
      // Add the original string to a character array.
      char[] arr = newS.ToCharArray();
      // Reverse the characters of the character array.
      Array.Reverse(arr);
      // Use a temp variable convert the character array into a string.
      string temp = new string(arr);
      // Extract the first half of the temp string.
      string second = temp.Substring(0, temp.Length / 2);

      // Validate if the first string matches the second string
      return first.Equals(second);
    }

  }
}
