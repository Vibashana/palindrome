﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace palindromeTest
{
  [TestClass]
  public class palindromeTest
  {
    [TestMethod]
    public void Main()
    {
      Check("", true);
      Check("a", true);
      Check("abcba", true);
      Check("abcde", false);
      Check("Mr owl ate my metal worm", true);
      Check("Never Odd Or Even", true);
      Check("Never Even Or Odd", false);
    }

    public void Check(string s, bool shouldBePalindrome)
    {
      //Console.WriteLine(IsPalindrome(s) == shouldBePalindrome ? "pass" : "FAIL");
      string result = (IsPalindrome(s) == shouldBePalindrome ? "pass" : "FAIL");
    }

    public bool IsPalindrome(string s)
    {
      // Remove all spaces from a string and force the string to uppercase
      string newS = (s.Replace(" ", "")).ToUpper();
      // Extract the first half of the original string.
      string first = newS.Substring(0, newS.Length / 2);
      // Add the original string to a character array.
      char[] arr = newS.ToCharArray();
      // Reverse the characters of the character array.
      Array.Reverse(arr);
      // Use a temp variable convert the character array into a string.
      string temp = new string(arr);
      // Extract the first half of the temp string.
      string second = temp.Substring(0, temp.Length / 2);

      // Validate if the first string matches the second string
      return first.Equals(second);
      //return palindrome.palindrome.validatePalindrome(s);
    }

  }
}
